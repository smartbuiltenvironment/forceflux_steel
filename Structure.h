#pragma once
#include "Common.h"

using namespace std;

class Structure
{

public:
	Structure();
	Structure(	int nA, 
				int nP, 
				int nZ, 
				int ppCount, 
				int paCount, 
				Vertex* vertices, 
				ArmIndices* arms, 
				float* massSize, 
				Vec3* loadVecs, 
				int* ppTop, 
				int* ppStartIndex, 
				int* ppRowLength, 
				int* paTop, 
				int* paStartIndex, 
				int* paRowLength, 
				bool* locked,
				bool* loaded,
				simulationType cType,
				GeometrySettings ms,
				SolverSettings solverSettings,
				MaterialSettings materialSettings,
				KernelSettings kernelSettings, 
				DisplacementSettings dispSettings,
				Stats stats,
				string suffix);

	~Structure();

	void SetupKernel(float alpha);
	void SetupLoad();
	void SetupSpring();
	void SetupPoissonsRatio();

	void CreateStructure(Vertex* vertices, ArmIndices* arms, float* massSize, Vec3* loadVecs, bool* locked, bool* loaded);
	void CopyTopology(int* ppTop, int* ppStartIndex, int* ppRowLength, int* pbTop, int* pbStartIndex, int* pbRowLength);

	void RunAnalysis();

	void ZeroParticleForce();
	void CalcArmForce();
	void CalcParticleMeanStrain();
	void CalcMass();
	void CalcDisplacementEnds();
	void CalcStrain();
	void SaveResults();

	void CalcStress();
	void CalcConvergenceForce();
	void CalcPoissonsRatio();
	void CalcPoissonsRatioVertical();
	void CalcSectionStress();
	void CountBrokenArms();
	void CreateGaugePlanes();

	void CalcFirstPartialVelocity();
	void CalcSecondPartialVelocity();
	
	void CalcAcceleration();
	void ApplyExternalSpringDisp();
	void TestConsitencyCondition();

	void OutputResults();
	void CollectData();
	void ExportModel();

public:
	std::vector<Arm> m_arms;
	std::vector<Particle> m_particles;
	std::vector<Vec3> m_pInitPos;
	std::vector<bool> m_locked;
	std::vector<bool> m_loaded;

	MaterialSettings m_material;
	DisplacementSettings m_disp;

	std::vector<LoadingCylinder> m_loadCylinders;
	std::vector<double> m_incDisp;
	std::vector<double> m_incStrain;
	std::vector<double> m_incGaugeStrain;

	std::vector<double> m_incStress;


	std::vector<gaugePlane> m_gaugePlane;

private:	
	
	KernelSettings m_kernel;
	SolverSettings m_solver;
	LoadSettings m_load;
	SpringSettings m_spring;
	PoissonsRatioSettings m_prs;
	GeometrySettings m_gs;
	
	Stats m_stats;

	int m_nParticles; 
	int m_nArms;
	int m_nZones;
	int m_nBrokenArms;

	int m_pClosestToOrigin;
	double m_gaugeSpacing;

	std::vector<Vec3> m_loadDirVecs;


	std::vector<double> m_avrgDispTotVer;
	std::vector<double> m_avrgDispTop;
	std::vector<double> m_avrgDispBot;

	std::vector<double> m_avrgDispGaugeTot;
	std::vector<double> m_avrgDispGaugeTop;
	std::vector<double> m_avrgDispGaugeBot;

	std::vector<double> m_avrgDispTotHor;
	std::vector<double> m_avrgDispLeft;
	std::vector<double> m_avrgDispRight;
	
	std::vector<double> m_incLoad;
	std::vector<double> m_incSpringForceTop;
	std::vector<double> m_incSpringForceBot;
	std::vector<double> m_incSectionStress;
	std::vector<double> m_incSectionStressCom;
	std::vector<double> m_incSectionStressTen;
	std::vector<double> m_reactionForceTop;
	std::vector<double> m_reactionForceBot;
	std::vector<double> m_incObfAvrg;
	std::vector<double> m_incPossionsRatio;

	std::vector<double> m_kernelConcistency;
	
	//Particle-particle topology
	int* m_ppTop;
	int* m_ppStartIndex;
	int* m_ppRowLength;
	int m_ppCount;

	bool m_plasticityOn;
	bool m_CheckFracture;
	BC m_bc;
	simulationType m_simType;

	std::vector<int> m_ppTopV;
	std::vector<int> m_ppStartIndexV;
	std::vector<int> m_ppRowLengthV;

	//Particle-arm topology
	int* m_paTop;
	int* m_paStartIndex;
	int* m_paRowLength;
	int m_paCount;

	std::vector<int> m_paTopV;
	std::vector<int> m_paStartIndexV;
	std::vector<int> m_paRowLengthV;

	string m_fileNameSuffix;

};


