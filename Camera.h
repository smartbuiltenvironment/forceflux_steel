#pragma once

#include "Common.h"

#ifdef VISUAL
#include "glm/glm.hpp" 
#include<GL/glew.h>
#include<glfw3.h>

using namespace glm;


class Camera 
{
public:
	Camera();
	Camera(float dist, Vec3 target);
	~Camera();
	void Initialise(float dist, Vec3 target);
	void ProcessKeyInput(keyAcion ka);
	void ProcessMouseInputRotate(double xPos, double yPos);
	void ProcessMouseInputPan(double xPos, double yPos);
	void ProcessScrollInput(float fov);
	void UpdateCameraCoord();
	Vec2 ScreenToWorldTransformation(float xPosScreen, float yPosScreen);

	float m_Distance;

	vec3 m_Position;
	vec3 m_Direction;
	vec3 m_Target;
	vec3 m_Up;
	vec3 m_Right;

	vec3 m_MoveX;
	vec3 m_MoveY;
	vec3 m_MoveZ;


	float m_MouseLastX, m_MouseLastY, m_MouseLastX_Pan, m_MouseLastY_Pan;
	float m_Yaw, m_Pitch;
	float m_Fov;
	bool m_FirstMouseRotate;
	bool m_FirstMousePan;
	float m_Speed;
	float m_ScreenWidth, m_ScreenHeight;

private:




};


#endif //VISUAL 
